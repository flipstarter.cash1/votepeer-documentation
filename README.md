# VotePeer Documentation

**[View Documentation](https://docs.voter.cash/)**

## Build

```bash
- pip install mkdocs
- pip install mkdocs-material
- pip install mkdocs-render-swagger-plugin
- npm build
```

## Contributing

To add document, simply create one and add it the nav section of `mkdocs.yml`.

View your document locally using `npm run serve`.

Before publishing, make sure the documents are approved by the markdown linter:

```bash
- npm install
- npm run lint
```

When your document is merged it will be automatically published by Gitlab CI.

## License

MIT
