# About

## Introduction

Censorship resistant on-chain blockchain voting. This is the documentation for the VotePeer project. The project originated from [Bitcoin Unlimited proposal #129](https://bitco.in/forum/threads/buip129-passed-finish-and-productize-the-bu-voting-system.24367).

The fundamental innovation in Votepeer compared to traditional online voting and polling systems is that VotePeer stores each vote on the bitcoin cash blockchain instead of storing votes in a centralized database. Using this design principle, VotePeer supports both transparent and anonymous elections. In transparent elections, everyone can see what everyone else voted in real-time. Votepeer is now being upgraded to support anonymous elections where nobody can see what the other participants voted while still tallying the result directly from the bitcoin cash blockchain to the VotePeer android app.

Compared to a traditional ballot box where participants write their choice on a piece of paper and trust the election administration to count the vote. VotePeer ensures that users can verify that the tally included their vote by using the VotePeer android app to read the ballot status and tally directly from the bitcoin cash blockchain. Trust, but verify.

The VotePeer android app is essentially a bitcoin wallet that interacts with on-chain bitcoin cash smart contracts. Upon opening the VotePeer android app for the first time, a mnemonic is generated and stored on the user's phone. An address from the mnemonic is automatically selected as the user's VotePeer blockchain identity.

When creating a new vote, all participants bitcoin cash addresses are required. Nobody can join the vote after it has started. If a participant attempts to vote twice, both votes are invalidated, and the participant's vote is set to "spoilt" (Invalid).
