# Building the Android App

> <https://gitlab.com/bitcoinunlimited/votepeer-android-app>

For building the debug build of the android app, you'll also need the two dependencies [votepeer-library](https://gitlab.com/bitcoinunlimited/votepeer-library) and [libbitcoincashkotlin](https://gitlab.com/bitcoinunlimited/libbitcoincashkotlin).

If you are building the release build, a pre-built version of these dependencies will be downloaded as part of the build. To do the release build, simply run `./gradlew assembleRelease`.

In this doc we'll focus on doing the debug build for developers.

## Before building

Clone the three projects in the same directory.

```bash
git clone https://gitlab.com/bitcoinunlimited/libbitcoincashkotlin.git
git clone https://gitlab.com/bitcoinunlimited/votepeer-library
git clone https://gitlab.com/bitcoinunlimited/votepeer-android-app.git
```

The relative path to each project is configured in `settings.gradle`.

Configure your SDK path by creating a file named `local.properties` in the Android app project root. It should look similar to this:

```dummy
## This file must *NOT* be checked into Version Control Systems,
# as it contains information specific to your local configuration.
#
# Location of the SDK. This is only used by Gradle.
# For customization when using a Version Control System, please read the
# header note.
#Fri Mar 26 15:37:10 CET 2021
sdk.dir=/Users/<user>/Library/Android/sdk
```

If you wish to add support for anonymous voting, you'll need to build with ring signature support. This requires more dependencies, see build guide for `libbitcoincashkotlin` for details. Add the following to enable ring signature support: `ringsignature=true`

If the build system is not finding your cmake installation, you can also explicitly add it to this file. Example:

```dummy
cmake.dir=/Users/<user>/cmake-3.19.7-macos-universal/CMake.app/Contents/
```

#### With INTELLIJ/Android studio

If you prefer to attempt the above within Android Studio, here is how to:

Navigate to `File -> Project form version control `, type in `https://gitlab.com/bitcoinunlimited/votepeer-android-app.git`.

Make sure Project SDk is added user project.

`File-> Project structure -> Project settings -> Project`

Make sure the latest Android SDK is selected under Project SDK

### Setup up Firebase

Follow this tutorial to setup a firebase project and retrieve a google-services.json file

<https://firebase.google.com/docs/android/setup>

add google-service.json file to: `app/google-services.json`

### Linters

This project uses [ktlint linter and code formatter](https://github.com/pinterest/ktlint). If using Android Studio, to make Intellij IDEA's built-in formatter produce ktlint-compatible code, run this once:

`./gradlew ktlintApplyToIDEAProject`

## Troubleshooting

### Android studio and cache

Android Studio is known for bad cache invalidation behavior causing build trouble. If you changed any of the settings above and get build issues, try removing its the cache.

`File -> Invalidate caches... -> Invalidate cashes and restart`

## Building the project

From console, run `./gradlew build`.

From android studio, in `Run/Debug Configuration`, add a `Gradle` task, set `build` as task.

### Other tasks

| Task | Description |
| ---- | ----------- |
| `ktlint` | Run linter and report errors |
| `ktlintFormat` | Run linter and attempt to fix errors |
| `test` | Run unit tests |

## Instrumental tests

See [VotePeer Android instrumental tests](android-test.md).
