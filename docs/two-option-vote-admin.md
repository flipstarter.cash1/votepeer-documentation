# Create a transparent two-option-vote election

## Prequesites

### VotePeer android app installed

Download the VotePeer app directly from GitLab or through google play.

<span style="left:1000px">
  <a align="center" href="https://gitlab.com/nerdekollektivet/votepeer-android-app/-/jobs/artifacts/master/raw/app-release.apk?job=assemble_release">
  <img src="apk-download-badge.png" width="27%">
  </a>

  <a align="center" href="https://play.google.com/store/apps/details?id=info.bitcoinunlimited.voting">
  <img src="https://cdn.rawgit.com/steverichey/google-play-badge-svg/master/img/en_get.svg" width="27%">
  </a>
</span>

## Open [VotePeer website](https://voter.cash)

It is recommended to use a desktop browser.

![](two-option-vote-admin-1.png)

## Open VotePeer Android app and open menu

![](two-option-vote-admin-3.png)

## Navigate to login

![](two-option-vote-admin-4.png)

## Press login button

![](two-option-vote-admin-5.png)

## Scan QR-code on the [VotePeer website](https://voter.cash)

![](two-option-vote-admin-6.png)
![](two-option-vote-admin-2.png)

Scan the QR-code. VotePeer logs you into the VotePeer website.

## [VotePeer website](https://voter.cash) loads and redirects

![](two-option-vote-admin-7.png)

## Press "Create a new election"-button

![](two-option-vote-admin-8.png)

## Create election

### Election type: Transparent

All participants vote transparent on the blockchain. Everyone can see what everyone votes.

Add

*   Description
*   Vote options
*   Participant addresses

### Visibility: Everyone

The election description, options, participants and tally is visible is to anyone with the link.

### Press "Create election"-button

![](two-option-vote-admin-9.png)

### Congratz! You have created a transparent and shareable election.

[Example election](https://voter.cash/#/election/pDT3LL01JtnzOUF6AyTn)

![](two-option-vote-admin-10.png)

### The participants will now receive a push notification in their VotePeer android apps

![](two-option-vote-admin-11.png)
