# Locating own anonymous ring signature vote

    Status: DRAFT
    Date: August 2021

In this document we describe how we locate a users own vote cast anonymously
using [traceable ring signature voting](ring-signature-vote-contract.md).

The goal of doing so is to:

1.  Know if a user has cast his vote in an election.
2.  For the user to know what they voted.

We do this by making the vote payload scriptpubkey deterministic. It is
possible to query for transactions using specific scriptpubkey using the
electrum server network. Using this method, we can locate our own vote
transaction from the blockchain.

**NOTE: This assumes the ring signature itself is deterministic. This
assumption is NOT VERIFIED.**

## Motivation

We want to learn this information from the blockchain, rather than relying
solely on storing it on the users local device.

The VotePeer android application uses 12-word recovery phrase (mnenomic) for
backup. We want to avoid needing additional backup steps to recover a user
identity and voting status, such as exporting/importing a file.

## Deterministic private/public key for payload

The [transaction input payload contract](input-payload-contract.md) used in
traceable ring signature voting for delivering the load requires a that we
use a unique private/public keypair. For the purpose of finding a users own
vote later, this needs to be deterministic.

We do this deterministically as follows:

    max_pk_value = FFFFFFFF00000000FFFFFFFFFFFFFFFFBCE6FAADA7179E84F3B9CAC2FC632551)
    i = 0
    do {
        election_private_key = SHA256(private_key || election_id || i)
        i += 1
    } until (election_private_key >= 1 and election_private_key <= max_pk_value)

Any number between >= 1 and <= `max_pk_value` is
[a valid secp256k1 private key](https://bitcoin.stackexchange.com/a/1715).

## Locating the voting transaction

Generate a payload for every vote option as described in
[ring-signature-vote-contract](ring-signature-vote-contract.md).

For each of the payloads, generate a `scriptpubkey` script as described in the
[transaction input payload contract](input-payload-contract.md).

Query an electrum server for transactions spending from each of the
scriptpubkey locking script. The first transaction by block height is a vote.
