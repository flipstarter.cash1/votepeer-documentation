# Media

Collection of articles and other media made about VotePeer.

## Articles

*   [VotePeer — A call for testers](https://bitcoinunlimit.medium.com/votepeer-a-call-for-testers-a0686c89ce6e) — *Bitcoin Unlimited Medium, March 2, 2021*
*   [Bitcoin Unlimited Launches Two-Option Voting App Powered by Bitcoin Cash](https://news.bitcoin.com/bitcoin-unlimited-launches-two-option-voting-app-powered-by-bitcoin-cash/) *— Bitcoin.com News, Sept 27, 2020*

## Audio & Video

*   [VotePeer: Anonymous vote on Bitcoin Cash demo](https://www.youtube.com/watch?v=NBhE_v6yewo), *13 Jan, 2022*
*   [Unlimit Podcast - VotePeer: Decentralised, censorship-resistant voting - Jørgen Svennevik Notland](https://www.youtube.com/watch?v=_MXrg3Ac8Ts), *28 Jan, 2021*

## Social media

*   [Satoshi's Angels weekly news mention](https://twitter.com/SatoshiAngels/status/1498258792781578240), *28 Feb, 2022*
*   [Bitcoin Unlimited Twitter Space discussion](https://twitter.com/BitcoinUnlimit/status/1484178894341083143), *Jan 20, 2022*
*   [CoinRiot hosts VotePeer developers at London Meetup](https://twitter.com/CoinRiot/status/1486353359141584901), *Jan 26, 2022*

## Notable events

*   [BitcoinUnlimited moves to using VotePeer for member elections](https://forum.bitcoinunlimited.info/t/voting-is-closed-for-buips-178-to-181/220), *Jan 21, 2022*
