# VotePeer authentication using Identity Protocol

VotePeer implements
[bchidentity protocol](http://bitcoinunlimited.net/identity__protocol) for user
authentication.

Any client supporting this protocol can login to the site.

This document *how* the bchidentity protocol is implemented in the VotePeer web
portal, where we use Firebase as the application backend.

## Overview

A user accessing the website login:

1.  Identifies using anonymous login.
2.  Requests a challenge.
3.  Signs the challenge using a Bitcoin Cash address of own choice.
4.  Submits to firebase for verification.
5.  Receives authentication token on successful verification.
6.  Authenticates using token.

## Detailed

First, the user initiates *anonymous login* with Firebase. Anonymous login
creates a temporary account that allows database access.

After a successful anonymous login, it's possible to proceed with
authentication using the
[bchidentity protocol](http://bitcoinunlimited.net/identity__protocol).

The user calls a REST call [request\_challenge](backend-functions.md), that is
implemented as a firebase function.

`request_challenge` creates a random string called *challenge*. The challenge
is stored in firebase together with the anonymous login identity. The challenge
is returned to the user.

The user then signs this challenge using a Bitcoin Cash address he wishes to
authenticate with. The user submits the address and signature using the REST
call [identify](backend-functions.md).

The `identify` REST call validates that the signature is signed with the
address provided. If the signature is valid, a firebase custom token is
returned to the user.

The token is stored locally in the users web browser, then a call to firebase
to authenticate with a custom token is performed. The anonymous login session
is "promoted" and the user has successfully authenticated using a Bitcoin Cash
address.
