# Background, goal and requirements

Bitcoin Unlimited uses Bitcoin Unlimited Improvement Proposals (BUIPs) to vote for projects. VotePeer was initiated and outlined by Bitcoin Unlimited’s lead developer Andrew Stone in [Bitcoin Unlimited Improvement Proposal 129 - BUIP129](https://bitco.in/forum/threads/buip129-passed-finish-and-productize-the-bu-voting-system.24367/). Through the legacy Bitcoin Unlimited [voting system](https://gitlab.com/bitcoinunlimited/votepeer-documentation).

From [BUIP129](https://bitco.in/forum/threads/buip129-passed-finish-and-productize-the-bu-voting-system.24367/):

> *This BUIP allocates resources to evolve this process into a fully-fledged voting system that could be easily adopted by external organizations, integrated with or closely related to the Bitcoin Cash blockchain.*

## End Goal, User Perspective

[BUIP129](https://bitco.in/forum/threads/buip129-passed-finish-and-productize-the-bu-voting-system.24367/) describes end goal for VotePeer based on the user's perspective.

*   > **A website allowing an external entity to register and manage a vote. This website will be open source and easily cloned and customized by institutions that want a dedicated site.**

Finalising the first version o the [VotePeer website](https://voter.cash/) cleared this goal. The VotePeer website’s web postal requires the VotePeer android app to log in.

*   > **A mobile “Activity” (embeddable in any mobile app) that handles voting, connecting to any registered entity. Embed this activity into an app that integrates with the website described above.**

The embeddable mobile "Activity" is implemented as the [VotePeer android library](https://gitlab.com/nerdekollektivet/votepeer-library). The library is implemented in [the VotePeer Android app](https://play.google.com/store/apps/details?id=info.bitcoinunlimited.voting). The app handles voting and integrates with the Votepeer website

### Requirements

As a subsection of end goals, requirements are selected from [Hjálmarsson et al.](https://skemman.is/bitstream/1946/31161/1/Research-Paper-BBEVS.pdf) Bold text style indicates that the requirement is solved using VotePeer with the [Two Option Vote (TOV) smart contract](https://docs.voter.cash/two-option-vote-contract). Traceable Ring Signature smart contracts resolves (ii). and (iii) is not resolved yet.

*   > **(i) An election system should not enable coerced voting**

*   > A stronger (or perhaps simply a clearer) form of (i) is needed to discourage selling of votes -- in traditional voting, someone can pay a voter to vote a certain way, but cannot verify what vote is actually cast. This inability to verify discourages direct vote purchases:

VotePeer with [Two Option Vote (TOV) smart contract](https://docs.voter.cash/two-option-vote-contract) is arguably more resistant to coerced voting as a dishonest voter cannot plausibly claim to have voted differently than they did.

*   > **(ii) An election system should not enable traceability of a vote to a voters identifying credentials**

Solved by using VotePeer [Traceable Ring Signatures](https://docs.voter.cash/ring-signature-vote-contract) Minimalistic and anonymous voting protocol on top of Bitcoin Cash. Participating, verifying and tallying does not require a full node, but is fully SPV client compatible. In combinatin with a [Transaction Input Payload Contract](https://docs.voter.cash/input-payload-contract/) for publishing arbitrary data payload on the Bitcoin Cash network using transaction input (scriptSig). Traceable Ring Signatures with Transaction Input Payload smart contracts are live on VotePeer now. A paper will be release to describe VotePeer anonymous voting.

*   > *(iii) An election system should ensure and proof to a voter, that the voters vote, was counted, and counted correctly.*

This requirement is not resolved yet.

*   > **(iv) An election system should not enable control to a third party to tamper with any vote.**

VotePeer stores all votes in BCH voting transactions using the TVO smart contract. Because the votes are stored in BCH transactions they are immutable and cannot be controlled or tampered with by a third party.

*   > **(v) An election system should not enable a single entity control over tallying votes and determining an elections result.**

Votes can salso be tallied directly from BCH using the android app or by opening a (transparrent and public VotePeer election)\[https://voter.cash/#/election/JDlT4ehYUG1AsrxcnDsB] in a browser
